import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from "@nestjs/microservices";

async function bootstrap() {
    const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
        transport: Transport.REDIS,
        options: {
            url: process.env.REDIS_URL,
            // queue: 'main_queue',
            // queueOptions: {
                // durable: false
            // }
        }
    });
    // app.setGlo
    await app.listen();
}

bootstrap().then(() => {

});
