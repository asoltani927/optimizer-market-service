import {HttpService} from '@nestjs/axios';
import {catchError} from 'rxjs';
import {HttpException} from '@nestjs/common';
import * as https from 'https';

export class Api {
    protected action: string;
    protected base: string;
    protected params: object;
    protected httpService: any;

    constructor(base: string) {
        this.params = {};
        this.base = base;
        // this.httpService = new HttpService();
        this.httpService = require('requests');
    }

    getUrl() {
        if (this.params) {
            const query = new URLSearchParams();
            Object.keys(this.params).forEach((key) => {
                query.append(key, this.params[key]);
            });
            return this.base + this.action + '?' + query.toString();
        }
        return this.base + this.action;
    }

    getResponse(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const url = this.getUrl();
                return await this.httpService(url, {
                    headers: {
                        accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    method: 'GET',
                    timeout: 50000,
                }).on('data', function (chunk) {
                    resolve({
                        status: 200,
                        data: JSON.parse(chunk)
                    })
                })
                    .on('end', function (err) {
                        if (err) return console.log('connection closed due to errors', err);
                    });
            } catch (e) {
                throw e;
            }
        });
    }

    protected addParam(name: string, value: any) {
        this.params[name] = value;
    }

    protected clear() {
        this.params = {};
    }

    protected setAction(path: string) {
        this.action = path;
    }
}
