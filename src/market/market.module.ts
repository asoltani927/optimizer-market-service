import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MarketController } from './market.controller';
import { MarketService } from './market.service';
import { MarketRadarRepository } from './repository/MarketRadar.repository';
import { GetMarketRadarDailySchedule } from './schedule/GetMarketRadarDaily.schedule';
import { MarketRadar, MarketRadarSchema } from './schema/marketRadar.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: MarketRadar.name,
        schema: MarketRadarSchema,
      },
    ]),
  ],
  providers: [
    MarketService,
    MarketRadarRepository,
    GetMarketRadarDailySchedule,
  ],
  controllers: [MarketController],
})
export class MarketModule {}
