import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MarketRadarDocument = MarketRadar & Document;

@Schema()
export class MarketRadar {
  @Prop({ required: true })
  balance: number;

  @Prop({ required: true })
  balanceChangeFiat: number;

  @Prop({ required: true })
  balanceInFiat: number;

  @Prop({ required: true })
  exchangeRate: number;

  @Prop({ required: true })
  totalBalanceChangeInFiat: number;

  @Prop({ required: true })
  totalBalanceInFiat: number;

  @Prop({ required: true })
  totalVolumeChangeInFiat: number;

  @Prop({ required: true })
  totalVolumeInFiat: number;

  @Prop({ required: true })
  transactionChange: number;

  @Prop({ required: true })
  transactionCount: number;

  @Prop({ required: true })
  userActivity: number;

  @Prop({ required: true })
  userActivityChange: number;

  @Prop({ required: true })
  volume: number;

  @Prop({ required: true })
  volumeChangeInFiat: number;

  @Prop({ required: true })
  volumeInFiat: number;

  @Prop({ default: Date.now })
  created_at: Date;

  @Prop({ default: Date.now })
  updated_at: Date;
}

export const MarketRadarSchema = SchemaFactory.createForClass(MarketRadar);
