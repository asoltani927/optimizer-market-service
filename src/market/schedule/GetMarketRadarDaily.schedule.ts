import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { MarketRadarRepository } from '../repository/MarketRadar.repository';
import { DappradarTool } from '../tools/dappradar.tool';

@Injectable()
export class GetMarketRadarDailySchedule {
  constructor(private marketRadarRepository: MarketRadarRepository) {}

  @Cron('0 0 * * * *', {
    name: 'market_radar_collector',
    // timeZone: process.env.TIMEZONE,
  })
  async trigger() {
    const response = await DappradarTool.get24H();
    if (response) {
      try {
        await this.marketRadarRepository.create({
          balance: response.balance,
          balanceChangeFiat: response.balanceChangeFiat,
          balanceInFiat: response.balanceInFiat,
          exchangeRate: response.exchangeRate,
          totalBalanceChangeInFiat: response.totalBalanceChangeInFiat,
          totalBalanceInFiat: response.totalBalanceInFiat,
          totalVolumeChangeInFiat: response.totalVolumeChangeInFiat,
          totalVolumeInFiat: response.totalVolumeInFiat,
          transactionChange: response.transactionChange,
          transactionCount: response.transactionCount,
          userActivity: response.userActivity,
          userActivityChange: response.userActivityChange,
          volume: response.volume,
          volumeChangeInFiat: response.volumeChangeInFiat,
          volumeInFiat: response.volumeInFiat,
        });
      } catch (e) {
        console.log(e);
      }
    }
  }
}
