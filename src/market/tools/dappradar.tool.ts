import { Api } from 'src/contracts/Api';

export class DappradarApi extends Api {
  public async get24H() {
    this.clear();
    this.setAction('/24h');
    this.addParam('currency', 'USD');
    const response = await this.getResponse();
    if (response.status === 200) {
      return response.data;
    }
    return null;
  }
}

export const DappradarTool = new DappradarApi(
  'https://dappradar.com/v2/api/dapp/multichain/marketplaces/opensea/statistic',
);
