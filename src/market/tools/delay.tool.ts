export class Delay {
  sleep = (ms: number) => {
    // @ts-ignore
    return new Promise((resolve) => setTimeout(resolve, ms));
  };
}

export const DelayTool = new Delay();
