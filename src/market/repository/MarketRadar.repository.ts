import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MarketRadarCreateDTO } from '../dto/MarketRadarCreate.dto';
import { MarketRadar, MarketRadarDocument } from '../schema/marketRadar.schema';

@Injectable()
export class MarketRadarRepository {
  constructor(
    @InjectModel(MarketRadar.name)
    private marketRadarModel: Model<MarketRadarDocument>,
  ) {}

  async create(
    createMarketRadarDTO: MarketRadarCreateDTO,
  ): Promise<MarketRadar> {
    const newMarketRadar = new this.marketRadarModel(createMarketRadarDTO);
    return await newMarketRadar.save();
  }

  async findAll(): Promise<MarketRadar[]> {
    return await this.marketRadarModel.find();
  }

  async getStatus(): Promise<number> {
    const response = await this.marketRadarModel
      .find()
      .limit(1)
      .sort({ $natural: -1 });
    try {
      const todayVolumeValue = parseFloat(
        response[0].totalVolumeInFiat.toString(),
      );
      const todayTransactionValue = parseFloat(
        response[0].transactionCount.toString(),
      );
      const todayUsersValue = parseFloat(response[0].userActivity.toString());
      const percentVolume = (todayVolumeValue * 100) / 160000000;
      const percentTransaction = (todayTransactionValue * 100) / 80000;
      const percentUsers = (todayUsersValue * 100) / 45000;
      const total =
        (percentTransaction * 50) / 100 +
        (percentVolume * 35) / 100 +
        (percentUsers * 15) / 100;
      return parseFloat(total.toFixed(2));
    } catch (e) {
      return 0;
    }
  }
}
