import { Controller, Get } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { MarketService } from './market.service';

@Controller('market')
export class MarketController {
  constructor(private readonly market_service: MarketService) {}

  @MessagePattern('market_status')
  async market_status() {
    const data = await this.market_service.getStatus();
    return {
      status: parseFloat(data.toString()),
    };
  }
}
