import { Injectable } from '@nestjs/common';
import { MarketRadarRepository } from './repository/MarketRadar.repository';

@Injectable()
export class MarketService {
  constructor(private repository: MarketRadarRepository) {}

  async getStatus() {
    return await this.repository.getStatus();
  }
}
