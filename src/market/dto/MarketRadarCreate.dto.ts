export interface MarketRadarCreateDTO {
  balance: number;
  balanceChangeFiat: number;
  balanceInFiat: number;
  exchangeRate: number;
  totalBalanceChangeInFiat: number;
  totalBalanceInFiat: number;
  totalVolumeChangeInFiat: number;
  totalVolumeInFiat: number;
  transactionChange: number;
  transactionCount: number;
  userActivity: number;
  userActivityChange: number;
  volume: number;
  volumeChangeInFiat: number;
  volumeInFiat: number;
}
